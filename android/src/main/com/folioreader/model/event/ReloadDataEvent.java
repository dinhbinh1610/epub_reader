package android.src.main.com.folioreader.model.event;

/**
 * This event is used when web page in {@link com.folioreader.ui.folio.fragment.FolioPageFragment}
 * is to reloaded.
 */
public class ReloadDataEvent {
}
