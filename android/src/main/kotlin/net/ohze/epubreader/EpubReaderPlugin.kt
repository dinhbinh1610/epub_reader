package net.ohze.epubreader

import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.PluginRegistry.Registrar

class EpubReaderPlugin(): MethodCallHandler {
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar): Unit {
      val channel = MethodChannel(registrar.messenger(), "epub_reader")
      channel.setMethodCallHandler(EpubReaderPlugin())
    }
  }

  override fun onMethodCall(call: MethodCall, result: Result): Unit {
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    } else {
      result.notImplemented()
    }
  }

//  override fun onOpenEpub (call:MethodCall, result: Result) {
//    if (call.method.equals("openEpub")) {
//      FolioReader folioReader = FolioReader.get()
//
//      folioReader.openBook(R.raw.adventures)
//
//      result.success(folioReader)
//    } else {
//      result.notImplemented()
//    }
//  }
}
